FROM node:8

# Create app directory
RUN mkdir -p /opt/nodeapp
WORKDIR /opt/nodeapp

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package.json /opt/nodeapp
RUN npm install

# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY index.js /opt/nodeapp 
COPY app /opt/nodeapp/app

EXPOSE 3000
EXPOSE 3306
CMD [ "npm", "start" ]