const Sequelize = require('sequelize')
const MappingModel = require('./mappings-model')

const database = process.env.DATABASE || 'urls_db'
const username = process.env.USER_NAME || 'root'
const password = process.env.PASSWORD || 'rootroot'
const dbhost = process.env.DB_HOST || 'localhost'
const dbport = process.env.DB_PORT || 3306
const dialect = process.env.DIALECT || 'mysql'

const sequelize = new Sequelize(database, username, password, {
  host: dbhost,
  port: dbport,
  dialect: dialect
})

const Model = MappingModel(sequelize, Sequelize)

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.')
  })
  .then(() => sequelize.sync())
  .then(() => {
    console.log('Tables are synced')
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err)
  })

module.exports = Model
