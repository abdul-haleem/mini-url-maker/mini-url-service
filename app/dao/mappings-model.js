module.exports = (mapper, orm) => {
  return mapper.define('mappings', {
    id: {type: orm.INTEGER, primaryKey: true, autoIncrement: true},
    url: orm.TEXT,
    urlHash: orm.STRING,
    nanoId: orm.STRING,
    accessedAt: {type: orm.DATE, defaultValue: orm.NOW}
  }, {
    indexes: [
      {
        unique: false,
        fields: ['urlHash']
      },
      {
        unique: false,
        fields: ['nanoId']
      },
      {
        unique: false,
        fields: ['accessedAt']
      }
    ]
  })
}
