const Model = require('./db-config')

class URLMappingDAO {
  async findOrCreate (url, urlHash, shortId) {
    const result = await Model.findOrCreate({where: {urlHash: urlHash}, defaults: {url: url, nanoId: shortId, accessedAt: Date.now()}})
    return result[0].dataValues.nanoId
  }

  async findByNanoId (shortId) {
    const result = await Model.findOne({where: {nanoId: shortId}})
    return result == null ? null : result.dataValues.url
  }
}
module.exports = URLMappingDAO
