const hash = require('string-hash')
const nanoid = require('nanoid')
const URLMappingDAO = require('../dao/url-mapping-dao')

class UrlMappingService {
  constructor () {
    this.shortIdLength = parseInt(process.env.ID_LENGTH) || 8
    this.baseUrl = process.env.BASE_URL || 'http://localhost:3000/'
    this.dao = new URLMappingDAO()
  }

  async shorten (url) {
    const urlHash = hash(url)
    const shortId = nanoid(this.shortIdLength)
    const shortIdFromDB = await this.dao.findOrCreate(url, urlHash, shortId)
    return this.baseUrl + shortIdFromDB
  }

  async getRedirectUrl (shortId) {
    return this.dao.findByNanoId(shortId)
  }
}

module.exports = UrlMappingService
