const UrlMappingService = require('./app/service/url-mapping-service')
const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const appPort = process.env.APP_PORT || 3000
const app = express()

app.use(cors())
app.use(bodyParser.json())

const service = new UrlMappingService()

app.post('/shorten', async (req, res) => {
  const url = req.body.url
  const shortUrl = await service.shorten(url)
  res.send({shortUrl})
})

app.get('/:nanoId', async (req, res) => {
  const url = await service.getRedirectUrl(req.params.nanoId)
  url == null ? res.status(404).end('<html>No result found</html>') : res.redirect(url)
})

app.listen(appPort)
