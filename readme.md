# INTRODUCTION
It is a NodeJs based rest service providing functionality to shorten a lenghty URL. When the shorten url is accessed, user is redirected to original URL.

# DEPENDENCIES
The servive used RDBMS for mapping lengthy urls to shorten urls. DB can be one of MySql, SQLite, PostGres, or MSSQL'. Specific DB can be set using environment variables at start up.

## ENVIRONMENT VARIABLES
Following environment variables need to be set while running the app.

### DATABASE
The database name. Default value is 'urls_db'

### USER_NAME
Database user name. Default value is 'root'

### PASSWORD
Database password. Default value is 'rootroot'

### DB_HOST
Database host. Default value is 'localhost'

### DB_PORT
The port DBMS is listening on. Default value is 3306.

### DIALECT
The RDBMS to use. Default value is 'mysql'. It can be one of 'mysql', 'sqlite', 'postgres', or 'mssql'

### APP_PORT
The port on which service should listen for incoming requests. Default value is 3000

### ID_LENGTH
Length of shortened part of url. Default value is 8

### BASE_URL
Shortened part of url is appended to this base url. Default value is 'http://localhost:3000/'. It should consist of the host where app is deployed and APP_PORT. It should end with "/".

# HOW TO RUN
To start app on local machine run 'npm run dev'

# END POINTS
App implements following end epoints

## POST /shorten
Shortens provided lengthy url.

### EXAMPLE
To test from Postman on local machine<br>
POST http://localhost:3000/shorten<br>
Under 'Body' select 'raw' and 'application/json' and provide following:<br>
{<br>
	"url": "http://docs.sequelizejs.com/manual/tutorial/models-usage.html#-find-search-for-one-specific-element-in-the-database"<br>
}

## GET /:nanoId
When short URL is pasted in any browser, this endpoint will be hit.